package tests

import (
	"encoding/json"
	"io"
	"local/app"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStatus(t *testing.T) {
	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/status", strings.NewReader(""))

	a := app.NewApp()

	a.ServeHTTP(w, r)

	assert.Equal(t, w.Result().StatusCode, http.StatusOK)

	body := w.Result().Body
	defer body.Close()

	b, err := io.ReadAll(body)
	assert.NoError(t, err)

	got := string(b)

	want, err := json.MarshalIndent(map[string]string{"status": "success"}, "", "\t")
	assert.NoError(t, err)

	assert.Equal(t, got, want)
}
