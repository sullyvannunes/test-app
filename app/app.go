package app

import (
	"encoding/json"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

type App struct {
	handler http.Handler
}

func NewApp() *App {
	app := &App{
		handler: routes(),
	}

	return app
}

func routes() http.Handler {
	m := httprouter.New()
	// m := http.NewServeMux()

	m.HandlerFunc(http.MethodGet, "/status", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		js, _ := json.MarshalIndent(map[string]string{"status": "success"}, "", "\t")

		w.Write(js)
	})

	return m
}

var _ http.Handler = (*App)(nil)

func (a *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.handler.ServeHTTP(w, r)
}
