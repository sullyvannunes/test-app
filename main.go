package main

import (
	"fmt"
	"local/app"
	"net/http"
)

func main() {
	a := app.NewApp()
	fmt.Println("HTTP Server listening on :3030")
	if err := http.ListenAndServe(":3030", a); err != nil {
		panic(err)
	}
}
